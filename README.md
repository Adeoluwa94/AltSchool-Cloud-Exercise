# **AltSchool-Cloud-Exercise**
## __Alt School Progress__

## **WEEK 1**

### __*Exercise 1*__
<li> ifconfig Task - Errors were shown so my screenshot showed 2 answers one was the error using dhcp and the other was with the static ip. 
<br>
When I understand linux better I would  come back and fix the dhcp error on my Mac.
<br>
<br>

### __*Exercise 2*__
<li> 10 Linux Command Task
<br>
<br>

### __*Exercise 3*__
<li> Timezone Task
<br>
<br>

### __*Exercise 4*__
<li> User, Group and SSH for Admin Group User
<br>
<br>

### __*Exercise 5*__
<li> Install PHP 7.4
<br>
<br>
